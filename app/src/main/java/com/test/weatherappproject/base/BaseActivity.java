package com.test.weatherappproject.base;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.test.weatherappproject.R;
import com.test.weatherappproject.WeatherProjApplication;

/**
 * Created by admin on 31/05/2018.
 */

public class BaseActivity extends AppCompatActivity {

    private ProgressDialog mProgressDialog;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        WeatherProjApplication.getApp().getDataComponent().inject(this);
        super.onCreate(savedInstanceState);

    }

    public void showDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            mProgressDialog.setCancelable(false);
            mProgressDialog.setMessage(getString(R.string.loading));
            mProgressDialog.show();
        } else {
            mProgressDialog.show();
        }
    }


    public void dismissDialog() {
        if (mProgressDialog != null) {
            mProgressDialog.dismiss();
        }
    }


}
