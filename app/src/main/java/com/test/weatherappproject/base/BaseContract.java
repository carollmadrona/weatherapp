package com.test.weatherappproject.base;

/**
 * Created by admin on 31/05/2018.
 */

public interface BaseContract {

    void showLoadingDialog();
    void hideLoadingDialog();
    void showErrorMessage(String errorMessage);


}
