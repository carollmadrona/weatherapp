package com.test.weatherappproject.viewmodel;

import com.test.weatherappproject.WeatherProjApplication;
import com.test.weatherappproject.base.BaseContract;
import com.test.weatherappproject.base.BasePresenter;
import com.test.weatherappproject.model.WeatherDetail;
import com.test.weatherappproject.usecase.GetWeatherDetail;
import com.test.weatherappproject.views.fragments.WeatherDetailFragment;

import javax.inject.Inject;

/**
 * Created by admin on 24/05/2018.
 */

public class GetWeatherDetailPresenter implements BasePresenter {

    @Inject
    GetWeatherDetail getWeatherDetail;

    private Contract view;

    private WeatherDetailFragment weatherDetailFragment;

    @Inject
    public GetWeatherDetailPresenter(WeatherDetailFragment weatherDetailFragment, Contract contract) {
        this.view = contract;
        this.weatherDetailFragment = weatherDetailFragment;
        WeatherProjApplication.getApp().getDataComponent().inject(this);
    }

    @Override
    public void dispose() {
        getWeatherDetail.dispose();
        view = null;
    }


    public interface Contract extends BaseContract {
        void getWeatherDetail(WeatherDetail weatherDetail);
    }

    public void getWeatherDetail(String locationId) {
        getWeatherDetail.execute(locationId).doFinally(() -> weatherDetailFragment.hideLoadingDialog()).doOnSubscribe(disposable -> weatherDetailFragment.showLoadingDialog()).subscribe(
                view::getWeatherDetail,
                t ->
                        view.showErrorMessage(t.getMessage())
        );
    }

}
