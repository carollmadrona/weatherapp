package com.test.weatherappproject.viewmodel;

import android.util.Log;

import com.test.weatherappproject.WeatherProjApplication;
import com.test.weatherappproject.base.BaseContract;
import com.test.weatherappproject.base.BasePresenter;
import com.test.weatherappproject.model.WeatherDetail;
import com.test.weatherappproject.model.WeatherResponse;
import com.test.weatherappproject.usecase.GetWeatherDetailFromCityName;
import com.test.weatherappproject.usecase.GetWeathers;
import com.test.weatherappproject.views.activities.MainActivity;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by admin on 24/05/2018.
 */

public class GetWeatherPresenter implements BasePresenter {

    private final MainActivity activity;
    private Contract view;
    @Inject
    GetWeathers getWeathers;

    @Inject
    GetWeatherDetailFromCityName getWeatherDetailFromCityName;

    @Inject
    public GetWeatherPresenter(MainActivity activity, Contract contract) {
        this.activity = activity;
        this.view = contract;
        WeatherProjApplication.getApp().getDataComponent().inject(this);
    }

    @Override
    public void dispose() {
        getWeathers.dispose();
        getWeatherDetailFromCityName.dispose();
        view = null;
    }

    public interface Contract extends BaseContract {
        void getWeatherResponse(WeatherResponse weatherResponse);

        void getWeatherDetailFromCityName(WeatherDetail weatherDetail);
    }


    public void getWeather(int[] ids) {
        List<Integer> mIds = new ArrayList<>();
        for (int id : ids) {
            mIds.add(id);
        }

        String idString = mIds.toString().replace(" ", "").replace("[", "").replace("]", "");
        Log.d("", "" + idString);
        getWeathers.execute(idString).doOnSubscribe(disposable -> activity.showDialog()).doFinally(activity::dismissDialog).subscribe(
                view::getWeatherResponse,
                t ->
                        view.showErrorMessage(t.getMessage())
        );

    }

    public void getWeatherDetailFromCityName(String cityName) {
        getWeatherDetailFromCityName.execute(cityName).doOnSubscribe(disposable -> activity.showDialog()).doFinally(activity::dismissDialog).subscribe(
                view::getWeatherDetailFromCityName,
                t ->
                        view.showErrorMessage(t.getMessage())
        );
    }


}
