package com.test.weatherappproject.viewmodel;

import android.app.Activity;
import android.content.Intent;

import com.test.weatherappproject.WeatherProjApplication;
import com.test.weatherappproject.base.BaseContract;
import com.test.weatherappproject.repository.Repository;
import com.test.weatherappproject.views.activities.MainActivity;

import javax.inject.Inject;

/**
 * Created by admin on 24/05/2018.
 */

public class SplashPresenter {

    private final Activity activity;

    @Inject
    Repository application;

    @Inject
    public SplashPresenter(Activity activity) {
        this.activity = activity;
        WeatherProjApplication.getApp().getDataComponent().inject(this);
    }


    public interface SplashContract extends BaseContract {
        void showLogo();
    }

    public void goToActivity() {
        activity.finish();
        Intent intent = new Intent(activity, MainActivity.class);
        activity.startActivity(intent);
    }

}
