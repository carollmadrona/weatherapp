package com.test.weatherappproject.usecase;

import android.content.Context;

import com.test.weatherappproject.R;
import com.test.weatherappproject.base.BaseUseCase;
import com.test.weatherappproject.executor.PostThreadExecutor;
import com.test.weatherappproject.executor.ThreadExecutor;
import com.test.weatherappproject.model.WeatherDetail;
import com.test.weatherappproject.repository.Repository;

import javax.inject.Inject;

import io.reactivex.Observable;

/**
 * Created by admin on 01/06/2018.
 */

public class GetWeatherDetailFromCityName extends BaseUseCase<WeatherDetail, String> {

    private final Repository mRepository;
    private final Context mContext;

    @Inject
    GetWeatherDetailFromCityName(Repository repository, ThreadExecutor threadExecutor, PostThreadExecutor postThreadExecutor, Context context) {
        super(threadExecutor, postThreadExecutor);
        this.mRepository = repository;
        this.mContext = context;
    }

    @Override
    protected Observable<WeatherDetail> buildUseCaseObservable(String s) {
        return mRepository.getWeatherFromCityName(s, mContext.getString(R.string.unit_metric));
    }

}
