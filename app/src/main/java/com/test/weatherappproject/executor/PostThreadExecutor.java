  package com.test.weatherappproject.executor;

import io.reactivex.Scheduler;

/**
 * Specify the thread in which the business logic callback is executed.
 */
public interface PostThreadExecutor {
  Scheduler getScheduler();
}
