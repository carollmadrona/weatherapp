package com.test.weatherappproject.executor;

import javax.inject.Inject;

import io.reactivex.Scheduler;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by admin on 31/05/2018.
 */

public class JobExecutor implements ThreadExecutor {

    @Inject
    JobExecutor() {
    }

    @Override public Scheduler getScheduler() {
        return Schedulers.io();
    }
}
