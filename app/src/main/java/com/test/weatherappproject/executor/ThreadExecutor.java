package com.test.weatherappproject.executor;

import io.reactivex.Scheduler;

/**
 * Specify the thread in which the business logic is executed.
 */
public interface ThreadExecutor {
  Scheduler getScheduler();
}
