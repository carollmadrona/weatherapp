package com.test.weatherappproject.executor;

import javax.inject.Inject;

import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;

/**
 * Created by admin on 31/05/2018.
 */

public class UIThread implements PostThreadExecutor {

    @Inject
    UIThread() {
    }

    @Override public Scheduler getScheduler() {
        return AndroidSchedulers.mainThread();
    }
}
