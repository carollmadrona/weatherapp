package com.test.weatherappproject;

import android.app.Application;

import com.test.weatherappproject.service.AppModule;

/**
 * Created by admin on 24/05/2018.
 */

public class WeatherProjApplication extends Application {

    private static WeatherProjApplication app;
    AppComponent dataComponent;

    public static WeatherProjApplication getApp() {
        return app;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        app = this;

        initDataComponent();

        dataComponent.inject(this);
    }

    private void initDataComponent(){
        dataComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .build();
    }

    public AppComponent getDataComponent() {
        return dataComponent;
    }
}