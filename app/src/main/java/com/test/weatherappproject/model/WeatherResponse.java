package com.test.weatherappproject.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by admin on 24/05/2018.
 */

public class WeatherResponse {
    @SerializedName("cnt")
    public int count;

    @SerializedName("list")
    public List<WeatherDetail> weatherDetailList;
}
