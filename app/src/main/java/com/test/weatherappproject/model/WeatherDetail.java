package com.test.weatherappproject.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by admin on 24/05/2018.
 */

public class WeatherDetail {

    @SerializedName("coord")
    public Coordinates coordinates;

    @SerializedName("weather")
    public List<Weather> weatherDetail;

    @SerializedName("main")
    public Temperature temperature;

    public Wind wind;

    public int id;

    public String name;

    public enum CityId{
        London(2643743), Seoul(1835848), NewYork(5128638), Tokyo(1850147);

        private final Integer mCityId;
        CityId(Integer cityId){
            mCityId = cityId;
        }

        public Integer getCityId(){
            return mCityId;
        }
    }
}
