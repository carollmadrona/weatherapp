package com.test.weatherappproject.model;

/**
 * Created by admin on 24/05/2018.
 */

public class Temperature {

    public float temp;
    public int pressure;
    public int humidity;
    public float temp_min;
    public float temp_max;
}
