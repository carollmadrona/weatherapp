package com.test.weatherappproject;

import com.google.gson.Gson;
import com.test.weatherappproject.base.BaseActivity;
import com.test.weatherappproject.repository.Repository;
import com.test.weatherappproject.service.AppModule;
import com.test.weatherappproject.viewmodel.GetWeatherPresenter;
import com.test.weatherappproject.viewmodel.SplashPresenter;
import com.test.weatherappproject.viewmodel.GetWeatherDetailPresenter;
import com.test.weatherappproject.views.fragments.DashboardFragment;
import com.test.weatherappproject.views.fragments.WeatherDetailFragment;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by admin on 24/05/2018.
 */

@Singleton
@Component(modules={AppModule.class})
public interface AppComponent {

    void inject(WeatherProjApplication retroRecyclerApplication);

    void inject(Gson gson);

    void inject(BaseActivity baseActivity);

    void inject(Repository repository);

    void inject(SplashPresenter splashPresenter);

    void inject(GetWeatherPresenter getWeatherPresenter);

    void inject(DashboardFragment dashboardFragment);

    void inject(WeatherDetailFragment weatherDetailFragment);

    void inject(GetWeatherDetailPresenter getWeatherDetailPresenter);

}