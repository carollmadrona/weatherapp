package com.test.weatherappproject.views.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.test.weatherappproject.R;
import com.test.weatherappproject.WeatherProjApplication;
import com.test.weatherappproject.base.BaseFragment;
import com.test.weatherappproject.model.Weather;
import com.test.weatherappproject.model.WeatherDetail;
import com.test.weatherappproject.model.WeatherResponse;
import com.test.weatherappproject.viewmodel.GetWeatherPresenter;
import com.test.weatherappproject.views.activities.MainActivity;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by admin on 24/05/2018.
 */

public class DashboardFragment extends BaseFragment implements GetWeatherPresenter.Contract {

    private static final String WEATHERRESPONSE_KEY = "WeatherResponse";
    private static final String CITYIDS_KEY = "CityIdsKey";
    private static final String URL = "http://openweathermap.org/img/w/";
    @Inject
    Gson gson;

    @BindView(R.id.list_weather)
    RecyclerView weatherList;

    @BindView(R.id.city_name)
    EditText mCityName;

    @BindView(R.id.empty_view)
    LinearLayout emptyView;

    private List<WeatherDetail> weatherDetailList;

    private int[] ids;

    private GetWeatherPresenter mainViewModel;

    public static DashboardFragment newInstance(WeatherResponse weatherResponse, int[] cityIds) {
        Bundle bundle = new Bundle();
        Gson gson = new Gson();
        bundle.putString(WEATHERRESPONSE_KEY, gson.toJson(weatherResponse));
        bundle.putIntArray(CITYIDS_KEY, cityIds);

        DashboardFragment fragment = new DashboardFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        WeatherProjApplication.getApp().getDataComponent().inject(this);
        super.onCreate(savedInstanceState);

        ids = getArguments().getIntArray(CITYIDS_KEY);
        weatherDetailList = new ArrayList<>();
    }

    @OnClick(R.id.btn_refresh)
    public void onRefresh() {
        if(!mCityName.getText().toString().equals("")){
            mCityName.getText().clear();
        } else {
            mainViewModel.getWeather(ids);
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        if(!mCityName.getText().toString().equals("")){
            mCityName.getText().clear();
        } else {
            mainViewModel.getWeather(ids);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_dashboard, container, false);

        ButterKnife.bind(this, view);

        mainViewModel = new GetWeatherPresenter((MainActivity) getActivity(), this);

        weatherList.setLayoutManager(new LinearLayoutManager(getContext()));
        WeatherResponse response = gson.fromJson(getArguments().getString(WEATHERRESPONSE_KEY), WeatherResponse.class);
        weatherList.setAdapter(new WeatherResponseAdapter(response.weatherDetailList));

        mCityName.setOnEditorActionListener((textView, i, keyEvent) -> {

            if (i == EditorInfo.IME_ACTION_SEARCH){
                hideKeyboard();
                mainViewModel.getWeatherDetailFromCityName(mCityName.getText().toString());
                return true;
            }

            return false;
        });

        mCityName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                if(mCityName.getText().toString().equals("")){
                    hideKeyboard();
                    mainViewModel.getWeather(ids);
                }
            }
        });

        return view;
    }


    @Override
    public void getWeatherResponse(WeatherResponse weatherResponse) {
        emptyView.setVisibility(View.GONE);
        weatherList.setVisibility(View.VISIBLE);
        weatherList.setAdapter(new WeatherResponseAdapter(weatherResponse.weatherDetailList));
    }

    @Override
    public void getWeatherDetailFromCityName(WeatherDetail weatherDetail) {
        emptyView.setVisibility(View.GONE);
        weatherList.setVisibility(View.VISIBLE);
        weatherDetailList.add(weatherDetail);
        weatherList.setAdapter(new WeatherResponseAdapter(weatherDetailList));
    }

    @Override
    public void showLoadingDialog() {
        showDialog();
    }

    @Override
    public void hideLoadingDialog() {

        dismissDialog();
    }

    @Override
    public void showErrorMessage(String errorMessage) {
        Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_SHORT).show();
    }

    public class WeatherResponseAdapter extends RecyclerView.Adapter<WeatherResponseAdapter.ViewHolder> {
        private List<WeatherDetail> mWeatherList;

        public class ViewHolder extends RecyclerView.ViewHolder {
            @BindView(R.id.city_name)
            TextView mCityName;

            @BindView(R.id.weather)
            TextView mWeather;

            @BindView(R.id.temp)
            TextView mTemp;

            @BindView(R.id.parent_layout)
            CardView mParentLayout;

            @BindView(R.id.icon_weather)
            ImageView mWeatherIcon;

            public ViewHolder(View view) {
                super(view);
                ButterKnife.bind(this, view);
            }
        }

        public WeatherResponseAdapter(List<WeatherDetail> mWeatherList) {
            this.mWeatherList = mWeatherList;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent,
                                             int viewType) {
            CardView v = (CardView) LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_location, parent, false);
            ViewHolder vh = new ViewHolder(v);
            return vh;
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            holder.mCityName.setText(mWeatherList.get(position).name);
            holder.mTemp.setText(String.format("%s %s", String.valueOf(mWeatherList.get(position).temperature.temp), "\u2103"));
            Glide.with(getActivity()).load(URL + mWeatherList.get(position).weatherDetail.get(0).icon + ".png").into(holder.mWeatherIcon);
            if (mWeatherList.get(position).weatherDetail.size() == 1) {
                holder.mWeather.setText(mWeatherList.get(position).weatherDetail.get(0).description);
            } else {
                String overallWeather = "";

                for (Weather weather : mWeatherList.get(position).weatherDetail) {
                    if (overallWeather.equals("")) {
                        overallWeather = weather.description;
                    } else {
                        overallWeather = overallWeather + "," + weather.description;
                    }
                }

                holder.mWeather.setText(overallWeather);
            }
            holder.mParentLayout.setOnClickListener(view -> {
                getFragmentManager().beginTransaction().addToBackStack("WEATHERDETAIL").replace(R.id.fragment_container, WeatherDetailFragment.newInstance(mWeatherList.get(position).name, mWeatherList.get(position))).commit();
            });


        }

        @Override
        public int getItemCount() {
            return mWeatherList.size();
        }
    }




}
