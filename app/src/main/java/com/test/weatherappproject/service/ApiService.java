package com.test.weatherappproject.service;

import com.test.weatherappproject.model.WeatherDetail;
import com.test.weatherappproject.model.WeatherResponse;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by admin on 24/05/2018.
 */

public interface ApiService {
    @GET("group")
    Observable<WeatherResponse> getWeathers(@Query("id") String ids, @Query("units") String units, @Query("APPID") String appId);
    //api.openweathermap.org/data/2.5/weather?q=London

    @GET("weather")
    Observable<WeatherDetail> getWeatherFromCityName(@Query("q") String cityName, @Query("units") String units, @Query("APPID") String appId);

    @GET("weather")
    Observable<WeatherDetail> getWeatherDetail(@Query("id") String id,@Query("units") String units, @Query("APPID") String appId);



}
