package com.test.weatherappproject.service;

import android.app.Application;
import android.content.Context;

import com.google.gson.Gson;
import com.test.weatherappproject.executor.JobExecutor;
import com.test.weatherappproject.executor.PostThreadExecutor;
import com.test.weatherappproject.executor.ThreadExecutor;
import com.test.weatherappproject.executor.UIThread;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by admin on 24/05/2018.
 */

@Module
public class AppModule {

    private static final String BASE_URL = "https://api.openweathermap.org/data/2.5/";

    private Application application;

    public AppModule(Application application) {
        this.application = application;
    }



    @Provides
    @Singleton
    Retrofit provideRetrofit(OkHttpClient okHttpClient) {
        Retrofit retrofit = new Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(BASE_URL)
                .client(okHttpClient)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
        return retrofit;
    }

    @Provides
    @Singleton
    public Gson providesGson(){
        return new Gson();
    }

    @Provides
    @Singleton
    public Context provideContext(){
        return application;
    }

    @Provides
    public ApiService provideService(Retrofit retrofit) {
        return retrofit.create(ApiService.class);
    }

    @Provides
    @Singleton
    public HttpLoggingInterceptor providesLoggingInterceptor(){
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        return interceptor;
    }

    @Provides
    @Singleton
    public OkHttpClient provideHttpClient(HttpLoggingInterceptor interceptor) {
        return new OkHttpClient().newBuilder().addInterceptor(interceptor).build();
    }

    @Provides @Singleton
    ThreadExecutor provideThreadExecutor(JobExecutor jobExecutor) {
        return jobExecutor;
    }

    @Provides @Singleton
    PostThreadExecutor providePostThreadExecutor(UIThread uiThread) {
        return uiThread;
    }


}
